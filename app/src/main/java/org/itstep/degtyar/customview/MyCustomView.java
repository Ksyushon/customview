package org.itstep.degtyar.customview;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import java.util.Random;

/**
 * Created by student on 18.01.2016.
 */
public class MyCustomView extends View {

    int counter;

    public MyCustomView(Context context) {
        super(context);
        counter = 0;

    }

    public MyCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyCustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MyCustomView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

    }


    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawARGB(255, 135, 189, 244);
        drawSun(canvas, 150, 150);

        drawCloud(canvas, canvas.getWidth() / 2, canvas.getHeight() / 7);
        drawLittleCloud(canvas, canvas.getWidth() / 6, canvas.getHeight() / 5);
        drawCloud(canvas, canvas.getWidth() / 3, canvas.getHeight() / 3);

        drawSeagull(canvas, canvas.getWidth() / 2, canvas.getHeight() / 2, 0, -140);
        drawSeagull(canvas, canvas.getWidth() / 3 - 50, canvas.getHeight() / 3, 0, -140);
        drawSeagull(canvas, canvas.getWidth() / 2 + 200, canvas.getHeight() / 7 + 200, 0, -140);

        drawShore(canvas, canvas.getWidth(), canvas.getHeight());
        drawSea(canvas, canvas.getWidth(), canvas.getHeight(), 200, 100);

        int cx = 10;
        int width = 40;
        int height = 30;
        int i = 0;
        int gup;

        for (int cy = canvas.getHeight() * 70 / 100 + 20; cy < canvas.getHeight() * 85 / 100; cy += 40) {
            if (i % 2 == 0) gup = 0;
            else gup = 20;
            drawTides(canvas, cx + gup, cy, width, height);
            i++;
        }


        Handler mHandler = new Handler();

         int endX = 0;

        endX+=counter;
        drawSail(canvas, endX, canvas.getHeight() * 70 / 100 - 60);
        if (endX <= canvas.getWidth()) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    counter += 2;
                    invalidate();
                }
            }, 1000 / 25);

        }
 else {
           counter = 0;
           invalidate();
       }

    }


    public void drawSun(Canvas canvas, float cx, float cy) {

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL_AND_STROKE);


        paint.setShader(new RadialGradient(cx, cy,
                cy / 3, Color.YELLOW, Color.argb(255, 247, 117, 31), Shader.TileMode.MIRROR));

        canvas.drawCircle(cx, cy, 50, paint);

        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2);
        canvas.drawLine(cx - 100, cy - 100, cx + 100, cy + 100, paint);
        canvas.drawLine(cx - 100, cy + 100, cx + 100, cy - 100, paint);
        canvas.drawLine(cx, cy - 130, cx, cy + 130, paint);
        canvas.drawLine(cx - 130, cy, cx + 130, cy, paint);
        canvas.drawLine(cx - 50, cy - 120, cx + 50, cy + 120, paint);
        canvas.drawLine(cx + 50, cy - 120, cx - 50, cy + 120, paint);
        canvas.drawLine(cx - 120, cy + 50, cx + 120, cy - 50, paint);
        canvas.drawLine(cx - 120, cy - 50, cx + 120, cy + 50, paint);


    }


    public void drawSeagull(Canvas canvas, float cx, float cy, int angle1, int angle2) {

        Paint paint = new Paint();
        RectF rectL = new RectF(cx, cy, cx + 60, cy + 50);
        RectF rectR = new RectF(cx + 60, cy + 10, cx + 120, cy + 50);
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(4);
        canvas.drawArc(rectL, angle1, angle2, false, paint);
        canvas.drawArc(rectR, angle1, angle2 - 20, false, paint);
    }




    public void drawCloud(Canvas canvas, float cx, float cy) {

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.argb(50, 196, 210, 221));

        canvas.drawOval(new RectF(cx, cy, cx + 100, cy + 50), paint);
        canvas.drawOval(new RectF(cx + 40, cy - 20, cx + 130, cy + 30), paint);
        canvas.drawOval(new RectF(cx + 70, cy - 40, cx + 180, cy + 50), paint);
        canvas.drawOval(new RectF(cx + 110, cy - 10, cx + 250, cy + 60), paint);
        canvas.drawOval(new RectF(cx + 30, cy + 10, cx + 180, cy + 80), paint);
        canvas.drawOval(new RectF(cx + 120, cy + 40, cx + 210, cy + 90), paint);
    }


    public void drawLittleCloud(Canvas canvas, float cx, float cy) {

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.argb(255, 18, 176, 213));

        paint.setColor(Color.argb(50, 196, 210, 221));
        canvas.drawOval(new RectF(cx, cy, cx + 100, cy + 50), paint);
        canvas.drawOval(new RectF(cx + 40, cy - 20, cx + 130, cy + 30), paint);
        canvas.drawOval(new RectF(cx + 70, cy - 40, cx + 180, cy + 50), paint);

    }


    public void drawSea(Canvas canvas, float cx, float cy, int lengthX, int lengthY) {

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.argb(255, 18, 176, 213));
        canvas.drawRect(0, cy * 70 / 100, cx, cy * 85 / 100, paint);

        paint.setStrokeWidth(10);
        RectF rectF = new RectF(0, cy * 85 / 100 - 30, cx, cy * 85 / 100 + 30);
        canvas.drawArc(rectF, 0, 180, false, paint);

        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.argb(150, 255, 255, 255));
        paint.setStrokeWidth(5);
        RectF rectL = new RectF(0, cy * 85 / 100 - 30, cx, cy * 85 / 100 + 30);
        canvas.drawArc(rectL, 0, 180, false, paint);

    }

    public void drawTides(Canvas canvas, float cx, float cy, int width, int height) {


        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.argb(50, 255, 255, 255));
        paint.setStrokeWidth(3);


        for (int i = 1; cx <= canvas.getWidth() - 10; i++) {

            if (i % 4 == 0) paint.setColor(Color.argb(255, 18, 176, 213));
            else paint.setColor(Color.argb(50, 255, 255, 255));

            canvas.drawArc(new RectF(cx, cy, cx + width, cy + height), 0, -180, false, paint);
            cx = cx + width;

        }

        invalidate();

    }

    public void drawShore(Canvas canvas, float cx, float cy) {

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.argb(255, 235, 219, 190));
        canvas.drawRect(0, cy * 85 / 100, cx, cy, paint);

    }


    public void drawSail(Canvas canvas, float cx, float cy) {

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.argb(255, 125, 72, 6));
        paint.setStrokeWidth(10);
        canvas.drawArc(new RectF(cx, cy, cx + 150, cy + 80), 0, 180, false, paint);

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.argb(250, 232, 75, 105));
        paint.setStrokeWidth(10);

        paint.setAntiAlias(true);

        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);

        path.moveTo(cx + 75, cy + 40);
        path.lineTo(cx + 75, cy - 150);
        path.lineTo(cx + 135, cy + 30);
        path.lineTo(cx + 75, cy + 40);
        canvas.drawPath(path, paint);
    }



}
